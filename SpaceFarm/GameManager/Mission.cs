﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public interface Mission
//{	
//	
//	bool CheckCompletion();
////	bool IsDone();
//}

public abstract class Mission: MonoBehaviour
{

//	public int id;
	public string name;
	public string title;
	[TextArea]
	public string description;
	[TextArea]
	public string requirements;
	[TextArea]
	public string endText;

	public abstract bool IsDone();
}