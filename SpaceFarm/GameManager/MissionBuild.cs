﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionBuild : Mission
{

	[Header("Parameters to check:")]
	public GameObject prefab;
	public int amount;

	private string prefabName;
	
	// Use this for initialization
	void Start () {
		InventoryItem prefabScript = prefab.GetComponent<InventoryItem>() as InventoryItem;
		prefabName = prefabScript.name;
	}
	

	public override bool IsDone()
	{
		int n = 0;
		foreach (GameObject item in InventoryManager.Instance.items)
		{
			InventoryItem itemScript = item.GetComponent<InventoryItem>() as InventoryItem;
			if (itemScript.name == prefabName)
			{
				++n;
			}
		}
		return n >= amount;
	}
}
