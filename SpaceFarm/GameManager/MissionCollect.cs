﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionCollect : Mission
{
	[Header("Compare collected resources with:")]
	public ResourceValues resourceAmount;

	public override bool IsDone()
	{
		return resourceAmount <= GameManager.Instance.gainedValues;
	}
}
