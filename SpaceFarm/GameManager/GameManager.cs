﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GameManager : MonoBehaviour
{
	public int Value = 0; // this variable is for test. delete it

//	public RawResources rawResources;
	public ResourceValues gainedValues = new ResourceValues(100, 100, 100);
//	public List<GameObject> Buildings;

	[HideInInspector]
	public GameObject researchLab;
	
	
	/*
	 * -------- part that makes it a singleton : 
	 * a static object that has to be created once anywhere in the game (this script has to be attached to an empty GameObject)
	 * and will stay present anywhere in the game
	 */
	private static GameManager instance;
	
	private void Awake()
	{
		if(instance==null) {
			instance = this;
			DontDestroyOnLoad(gameObject);	// this very line makes class a static instant
		}
		else
		{
			Destroy(gameObject);
		}
	}
	
	public static GameManager Instance {
		get {
			if(instance==null) {
				instance = new GameManager();
			}
 
			return instance;
		}
	}
	/*
	 * ---------- end of singleton essentials
	 */
	

	private GameManager() {
		// initialize your game manager here. Do not reference to GameObjects here (i.e. GameObject.Find etc.)
		// because the game manager will be created before the objects
	}    
 
	
 
	// Add your game mananger members here
	public void Pause(bool paused) {
	}

	
	
	public void AddCarbon(int v)
	{
//		resources.carbon += v;
	}

	public void AddResources(ResourceValues resources)
	{
		gainedValues += resources;
	}
	
	public void SubstructResources(ResourceValues resources)
	{
		gainedValues -= resources;
	}

}
