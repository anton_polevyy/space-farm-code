﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionManager : MonoBehaviour
{


	public List<Mission> missions;	
	
	[HideInInspector]
	public List<bool> missionIsDone;

	
	/*
	 * ----------- part that makes it a singleton : 
	 * a static object that has to be created once anywhere in the game (this script has to be attached to an empty GameObject)
	 * and will stay present anywhere in the game  ⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄⌄
	 */
	public static MissionManager Instance { get; private set; }

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			Destroy(this.gameObject);
		}
		
		/* ⌃⌃⌃⌃⌃⌃⌃⌃⌃⌃ end of singleton essentials ------------ */
		
		
		/* Initial declaration of missionIsDone shell be in Awake(),
		 so it would be ready by the time MissionUI's Start() will start using it */
		for (int i = 0; i < missions.Count; i++)
			missionIsDone.Add(false);
	}
	

	void Update ()
	{
		UpdateMissionsCompletionStatus();
	}


	
	private void UpdateMissionsCompletionStatus()
	{
		for (int i = 0; i < missionIsDone.Count; i++)
		{
//			/* updates missionHasBeenDone to True only once, then skips it */
//			if (!missionIsDone[i]) missionIsDone[i] = missions[i].IsDone();
			
			/* updates missionHasBeenDone all the time */
			missionIsDone[i] = missions[i].IsDone();
		}
	}

}
