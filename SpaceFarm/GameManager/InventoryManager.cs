﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityScript.Lang;





public class InventoryManager : MonoBehaviour
{
	
	// create two classes ItemBuilding and ItemDroid, both are children of InventoryItem.
	// ItemBuilding shell Produce Resources every Time stamp
	// ItemDroid shell have action functions 

	public List<GameObject> items;
//	public List<GameObject> buildings;
//	public List<GameObject> droids;
	
	
	/*
	 * -------- part that makes it a singleton : 
	 * a static object that has to be created once anywhere in the game (this script has to be attached to an empty GameObject)
	 * and will stay present anywhere in the game
	 */
	public static InventoryManager Instance { get; private set; }

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			Destroy(this.gameObject);
		}
	}
	
	/*
	 * ---------- end of singleton essentials
	 */

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
