﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

[System.Serializable]
public struct SoilType
{
	public string name;
	[TextArea]
	public string description;
	public float clayMin;
	public float clayMax;
	public float siltMin;
	public float siltMax;
	public float sandMin;
	public float sandMax;
	
}

public class ItemLabAnaliticsUI : MonoBehaviour, IPointerDownHandler, 
	IBeginDragHandler, IDragHandler, IEndDragHandler
{

	public Color activeTabColor;
	public Color inactiveTabColor;
	public Color enabledTextColor;
	public Color disabledTextColor;

	[Header("Refining Tab")] 
	public Button btnRefiningTab;
	public Slider waterOxygenSlider;
	public Text waterOxygenRatio;
	public Slider carbonNitrogenSlider;
	public Text carbonNitrogenRatio;
	public Slider carbonMetalSlider;
	public Text carbonMetalRatio;

	[Header("Soil Tab")] 
	public Button btnSoilTab;
	public Slider waterSlider;
	public Text waterPercentText;
	public Slider airSlider;
	public Text airPercentText;
	public Slider biomassSlider;
	public Text biomassPercentText;
	public Slider mineralsSlider;
	public Text mineralsPercentText;
	
//	public Slider totalSlider;
	public Text totalPercentText;
	public Text soilCompositionText;
	public string messageSuccess;
	public string messageFail;
	public Button btnCreate;
	public Text txtBtnCreate;

	[Header("Check components range (%)")] 
	public int waterMin = 20;
	public int waterMax = 30;
	public int airMin = 20;
	public int airMax = 30;
	public int mineralsMin = 43;
	public int mineralsMax = 47;
	public int biomassMin = 4;
	public int biomassMax = 6;

	[Header("Soil Type Tab")] 
	public Button btnSoilTypeTab;
	public Text txtSoilTypeTab;

	public GameObject circleUI;
	public GameObject pointerInCircle;
	public Slider sandSlider;
	public Text sandPercentText;
	public Slider siltSlider;
	public Text siltPercentText;
	public Slider claySlider;
	public Text clayPercentText;
	public Text totalSoilTypeText;
	public Button btnCreateSoilType;
	public Text txtBtnCreateSoilType;
	public Button btnResetSoilType;
	[Header("Soil Type Description")] 
	public TextMeshProUGUI soilName;
	public TextMeshProUGUI soilDescription; 
	
	public List<SoilType> soilTypes;
	

	private GameObject currentLab;
	private int totalSoilPercentage;
	private float totalSoilType;
	
	


	// Use this for initialization
	void Start ()
	{
		/* currentLab feature was implemented in case there are several labs,
		 * but looks like it should be removed */
		currentLab = GameManager.Instance.researchLab;
		
		/* set Soil Type tab inactive (until the right Soil Composition is created) */
		btnSoilTypeTab.interactable = false;
		txtSoilTypeTab.color = disabledTextColor;
		
		/* Set listeners on Tab Buttons click */
		btnRefiningTab.onClick.AddListener(delegate { OnTabButtonClick(btnRefiningTab, btnSoilTab, btnSoilTypeTab); });
		btnSoilTab.onClick.AddListener(delegate { OnTabButtonClick(btnSoilTab, btnRefiningTab, btnSoilTypeTab); });
		btnSoilTypeTab.onClick.AddListener(delegate { OnTabButtonClick(btnSoilTypeTab, btnRefiningTab, btnSoilTab); });
		
		/* ------------------ Refining Tab ----------------------- */
		
		/* Set starting Raw Resources slider values */
		waterOxygenSlider.value = 0.5f;
		carbonNitrogenSlider.value = 0.5f;
		carbonMetalSlider.value = 0.5f;
		
		waterOxygenSlider.onValueChanged.AddListener(delegate
		{
			OnSliderChangeRatio(waterOxygenSlider, waterOxygenRatio);
			RefineWaterOxygenRatio();
		});
		
		carbonNitrogenSlider.onValueChanged.AddListener(delegate
		{
			OnSliderChangeRatio(carbonNitrogenSlider, carbonNitrogenRatio);
			RefineCarbonNitrogenRatio();
		});
		
		carbonMetalSlider.onValueChanged.AddListener(delegate
		{
			OnSliderChangeRatio(carbonMetalSlider, carbonMetalRatio);
			RefineCarbonMetalsRatio();
		});
		
		
		/* --------------- Soil Composition Tab ---------------------- */
		
		waterPercentText.text = "" + waterSlider.value + "%";
		airPercentText.text = "" + airSlider.value + "%";
		biomassPercentText.text = "" + biomassSlider.value + "%";
		mineralsPercentText.text = "" + mineralsSlider.value + "%";
		
		
		//Adds a listener to a slider and invokes a method when the value changes.
		waterSlider.onValueChanged.AddListener(delegate {OnSliderChangeText(waterSlider, waterPercentText); });
		airSlider.onValueChanged.AddListener(delegate {OnSliderChangeText(airSlider, airPercentText); });
		biomassSlider.onValueChanged.AddListener(delegate {OnSliderChangeText(biomassSlider, biomassPercentText); });
		mineralsSlider.onValueChanged.AddListener(delegate {OnSliderChangeText(mineralsSlider, mineralsPercentText); });

		updateTotalPercentage();
		
		
		/* ----------------- Soil Type Tab ---------------------- */
		
		sandSlider.value = 0;
		siltSlider.value = 0;
		claySlider.value = 0;
		
		sandPercentText.text = "" + sandSlider.value + "%";
		siltPercentText.text = "" + siltSlider.value + "%";
		clayPercentText.text = "" + claySlider.value + "%";
		
		sandSlider.onValueChanged.AddListener(delegate {OnSliderChangeSoilTypePercentage(sandSlider, sandPercentText); });
		siltSlider.onValueChanged.AddListener(delegate {OnSliderChangeSoilTypePercentage(siltSlider, siltPercentText); });
		claySlider.onValueChanged.AddListener(delegate {OnSliderChangeSoilTypePercentage(claySlider, clayPercentText); });

		btnResetSoilType.onClick.AddListener(delegate
		{
			Debug.Log("delegating btnResetSoilType");
			ResetSoilType();
		});
		
		btnCreateSoilType.onClick.AddListener(delegate
		{
			int i = GetSoilTypeIndex();
			if (i == -1) Debug.Log("Error: No matching Soil Type");
			soilName.SetText(soilTypes[i].name);
			soilDescription.SetText(soilTypes[i].description);
		});
		
		updateTotalSoilTypePercentage();
	}
	

	private void OnTabButtonClick(Button activeTab, Button inactiveTab1, Button inactiveTab2)
	{
		ColorBlock activeColors = activeTab.colors;
		ColorBlock inactiveColors = inactiveTab1.colors;

		activeColors.normalColor = activeTabColor;
		activeColors.highlightedColor = activeTabColor;
		activeColors.pressedColor = activeTabColor;
		activeTab.colors = activeColors;
		
		inactiveColors.normalColor = inactiveTabColor;
		inactiveColors.highlightedColor = inactiveTabColor;
		inactiveColors.pressedColor = inactiveTabColor;
		inactiveTab1.colors = inactiveColors;
		inactiveTab2.colors = inactiveColors;

	}


	/* ---------------------------- Refine Tab --------------------------------------------- */
	
	
	private void RefineWaterOxygenRatio()
	{
		ItemLab labScript = (ItemLab)currentLab.GetComponent(typeof(ItemLab));
		if (labScript != null)
		{
			labScript.waterToOxygenRatio = waterOxygenSlider.value;
			labScript.oxygenToWaterRatio = (1 - waterOxygenSlider.value);
		}
	}
	
	private void RefineCarbonNitrogenRatio()
	{
		ItemLab labScript = (ItemLab)currentLab.GetComponent(typeof(ItemLab));
		if (labScript != null)
		{
			labScript.carbonToNitrogenRatio = carbonNitrogenSlider.value;
			labScript.nitrogenToCarbonRatio = (1 - carbonNitrogenSlider.value);
		}
	}
	
	private void RefineCarbonMetalsRatio()
	{
		ItemLab labScript = (ItemLab)currentLab.GetComponent(typeof(ItemLab));
		if (labScript != null)
		{
			labScript.carbonToMetalsRatio = carbonMetalSlider.value;
			labScript.metalsToCarbonRatio = (1 - carbonMetalSlider.value);
		}
	}
	
	private void OnSliderChangeRatio(Slider slider, Text text)
	{
		text.text = "" + (int)Math.Round(slider.value * 10) + "/" + (int)Math.Round(10 - slider.value * 10);
	}
	
	
	
	/* ---------------------------- Soil Tab --------------------------------------------- */

	
	private void OnSliderChangeText(Slider slider, Text text)
	{
		text.text = "" + slider.value + "%";
		updateTotalPercentage();
	}


	private void updateTotalPercentage()
	{
		
		totalSoilPercentage = (int)(waterSlider.value + airSlider.value + biomassSlider.value + mineralsSlider.value);
		totalPercentText.text = "" + totalSoilPercentage + "%";

		if (totalSoilPercentage == 100)
		{
			btnCreate.interactable = true;
			txtBtnCreate.color = btnCreate.colors.normalColor;
		}
		else
		{
			btnCreate.interactable = false;
			txtBtnCreate.color = btnCreate.colors.disabledColor;
		}
	}

	public void OnCreateSoilButton()
	{
		if (IsSoilComposition())
		{
//			Debug.Log("soil fits the composition");
			soilCompositionText.text = messageSuccess;
			btnSoilTypeTab.interactable = true;
			txtSoilTypeTab.color = enabledTextColor;
		}
		else
		{
//			Debug.Log("is not soil");
			soilCompositionText.text = messageFail;
			btnSoilTypeTab.interactable = false;
			txtSoilTypeTab.color = disabledTextColor;
		}
	}

	private bool IsSoilComposition()
	{
		bool isAnswer = true;

		isAnswer = (waterSlider.value >= waterMin && waterSlider.value <= waterMax) ? isAnswer : false;
		isAnswer = (airSlider.value >= airMin && airSlider.value <= airMax) ? isAnswer : false;
		isAnswer = (biomassSlider.value >= biomassMin && biomassSlider.value <= biomassMax) ? isAnswer : false;
		isAnswer = (mineralsSlider.value >= mineralsMin && mineralsSlider.value <= mineralsMax) ? isAnswer : false;
		isAnswer = (totalSoilPercentage == 100) ? isAnswer : false;

		return isAnswer;
	}
	
	
	
	/* ---------------------------- Soil Type Tab --------------------------------------------- */
	
	public void OnPointerDown(PointerEventData pointer)
	{

		if (IsPointerOnUI(pointer, circleUI))
		{
			OnPointerAtWheelChangeSliders(pointer, circleUI, claySlider, siltSlider, sandSlider);
		}
	}

	public void OnDrag(PointerEventData pointer)
	{
//		Debug.Log("OnDrag()");
		if (IsPointerOnUI(pointer, circleUI))
		{
			OnPointerAtWheelChangeSliders(pointer, circleUI, claySlider, siltSlider, sandSlider);
		}
	}
	
	private bool IsPointerOnUI(PointerEventData pointer, GameObject uiObject)
	{		
		Vector3 pointerPos = new Vector3(pointer.position.x, pointer.position.y, 0);
		
		GraphicRaycaster graphicRaycaster;
        
		//Fetch the Raycaster from the GameObject (the Canvas)
		graphicRaycaster = GetComponent<GraphicRaycaster>();
        
		//Create a list of Raycast Results
		List<RaycastResult> results = new List<RaycastResult>();
        
		//Raycast using the Graphics Raycaster and mouse click position
		graphicRaycaster.Raycast(pointer, results);

		//For every result returned, output the name of the GameObject on the Canvas hit by the Ray
		foreach (RaycastResult result in results)
		{
			if (result.gameObject == uiObject) return true;
		}
		return false;
	}

	private void OnPointerAtWheelChangeSliders(PointerEventData pointer, GameObject circleUI, 
												Slider slider1, Slider slider2, Slider slider3)
	{
		Debug.Log("pointer position: " + pointer.position);
		Debug.Log(" circleUI center: " + circleUI.transform.position);
		
		/* worl corners have 4 points, but to get the radius
		 we only need to get the difference between the center and one of the sides 
		 (assuming that the image of a circle UI is square)
		 */
		Vector3 center = circleUI.transform.position;
		Vector3[] v = new Vector3[4];
		circleUI.GetComponent<RectTransform>().GetWorldCorners(v);

		float radius =  Mathf.Abs(center.x - v[0].x);
		Debug.Log("radius is " + radius);

		/* 3 vectors from the center of the wheel are Clay, Silt and Sand
			one vector goes up 90 degrees and two others go 210 and 330 degrees
			Random dot in this circle makes a vector from the center
			projection of the vector on these three vectors gives us the value of Clay, Silt and Sand
		 */
		
		// make vector from point and the center of the wheel
		Vector2 vec = new Vector2(pointer.position.x - center.x, pointer.position.y - center.y);
		
		// make sure vector doesn't go outside the wheele
		if (vec.magnitude > radius) vec = vec * radius / vec.magnitude;
		
		// update pointerInCircle position
		pointerInCircle.transform.position = new Vector2(center.x + vec.x, center.y + vec.y);
		
		// Get three basis vectors from the wheel center with the length of radius and angle of 90, 210 and 330 degrees
		Vector2 vec1 = new Vector2(radius * Mathf.Cos(90 * Mathf.Deg2Rad), radius * Mathf.Sin(90 * Mathf.Deg2Rad));
		Vector2 vec2 = new Vector2(radius * Mathf.Cos(210 * Mathf.Deg2Rad), radius * Mathf.Sin(210 * Mathf.Deg2Rad));
		Vector2 vec3 = new Vector2(radius * Mathf.Cos(330 * Mathf.Deg2Rad), radius * Mathf.Sin(330 * Mathf.Deg2Rad));
		
		/* If extend vectors to the point of projection of vectors on one another,
			then these vectors are basically heights of an equilateral triangle in a circle.
			Therefore, the length (magnitude) of the vectors (triangle heights) is 3/2 of circle radius.
				to put it in formula: vec1 = (vec1 / vec1.magnitude) * 1.5 * radius
			but it takes more than just to extend it. Vector would also need to have another starting point, 
			thus I just compare the projection with the desirable length 
			and make a conclusion how much of Clay, Silt and Sand should go into the combination for Soil Type
		 */		
		
		// find projection of the vector (point in soil wheel) to these three vectors
		float proj1 = (vec.x * vec1.x + vec.y * vec1.y) / Mathf.Sqrt(vec1.x * vec1.x + vec1.y * vec1.y);
		float proj2 = (vec.x * vec2.x + vec.y * vec2.y) / Mathf.Sqrt(vec2.x * vec2.x + vec2.y * vec2.y);
		float proj3 = (vec.x * vec3.x + vec.y * vec3.y) / Mathf.Sqrt(vec3.x * vec3.x + vec3.y * vec3.y);

		proj1 = (proj1 >= 0) ? proj1 + radius / 2 : (-proj1 < radius / 2) ? radius / 2 + proj1 : 0;
		proj2 = (proj2 >= 0) ? proj2 + radius / 2 : (-proj2 < radius / 2) ? radius / 2 + proj2 : 0;
		proj3 = (proj3 >= 0) ? proj3 + radius / 2 : (-proj3 < radius / 2) ? radius / 2 + proj3 : 0;
		
		
		
		/* Now, whatever amount of Clay, Silt and Sand we got, take the sum as 100% and find proportios */
		float sum = proj1 + proj2 + proj3;
		
		proj1 = Mathf.Round(proj1 / sum * 1000f) / 1000f;
		proj2 = Mathf.Round(proj2 / sum * 1000f) / 1000f;
		proj3 = Mathf.Round(proj3 / sum * 1000f) / 1000f;
		
		Debug.Log("proj1 = " + proj1 + ", proj2 = " + proj2 + ", proj3 = " + proj3);

		// current precaution is to make sure the sum will be 100%
		sum = proj1 + proj2 + proj3;
		if (sum < 1 || sum > 1) proj1 = 1 - proj2 - proj3;

		// adjust sliders
		slider1.normalizedValue = proj1;
		slider2.normalizedValue = proj2;
		slider3.normalizedValue = proj3;

	}

	private void OnSliderChangeSoilTypePercentage(Slider slider, Text text)
	{
		slider.value = Mathf.Round(slider.value * 10f) / 10f;
		text.text = "" + slider.value + "%";
		updateTotalSoilTypePercentage();
	}
	
	private void updateTotalSoilTypePercentage()
	{
		
		totalSoilType = Mathf.Round((sandSlider.value + siltSlider.value + claySlider.value) * 100f) / 100f;
		totalSoilTypeText.text = "" + totalSoilType + "%";

		if (totalSoilType == 100)
		{
			btnCreateSoilType.interactable = true;
			txtBtnCreateSoilType.color = btnCreate.colors.normalColor;
		}
		else
		{
			btnCreateSoilType.interactable = false;
			txtBtnCreateSoilType.color = btnCreate.colors.disabledColor;
		}
	}

	public void ResetSoilType()
	{
		Debug.Log("ResetSoilType()");
		sandSlider.value = 0;
		siltSlider.value = 0;
		claySlider.value = 0;
	}
	
	private int GetSoilTypeIndex()
	{
		for (int i = 0; i < soilTypes.Count; i++)
		{
			if (sandSlider.value >= soilTypes[i].sandMin && sandSlider.value <= soilTypes[i].sandMax
			                                     && siltSlider.value >= soilTypes[i].siltMin 
			                                     && siltSlider.value <= soilTypes[i].siltMax
			                                     && claySlider.value >= soilTypes[i].clayMin 
			                                     && claySlider.value <= soilTypes[i].clayMax)
				return i;
		}

		return -1;
	}
	
	
	
}
