﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResourcesUI : MonoBehaviour
{
	public Slider sliderHeat;
	public Slider sliderAir;
	
    public Text iceAmount;
    public Text rocksAmount;
    public Text mineralsAmount;
	
    public Text waterAmount;
	public Text oxygenAmount;
	public Text carbonAmount;
    public Text nitrogenAmount;
    public Text metalsAmount;
	
    public Text biomassAmount;
    public Text rationsAmount;
    public Text soilAmount;
//    public Text textAir;
	
	
	
    // Use this for initialization
    void Start () {
		
    }
	
    // Update is called once per frame
    void Update ()
    {

	    sliderHeat.value = (float)GameManager.Instance.gainedValues.heat / 100;
	    sliderAir.value = (float)GameManager.Instance.gainedValues.air / 100;
		
        if(iceAmount != null) iceAmount.text = "" + GameManager.Instance.gainedValues.ice;
        if(rocksAmount != null) rocksAmount.text = "" + GameManager.Instance.gainedValues.rocks;
        if(mineralsAmount != null) mineralsAmount.text = "" + GameManager.Instance.gainedValues.minerals;
		
        if(waterAmount != null) waterAmount.text = "" + GameManager.Instance.gainedValues.water;
        if(nitrogenAmount != null) nitrogenAmount.text = "" + GameManager.Instance.gainedValues.nitrogen;
        if(oxygenAmount != null) oxygenAmount.text = "" + GameManager.Instance.gainedValues.oxygen;
        if(carbonAmount != null) carbonAmount.text = "" + GameManager.Instance.gainedValues.carbon;
        if(metalsAmount != null) metalsAmount.text = "" + GameManager.Instance.gainedValues.metal;
		
        if(biomassAmount != null) biomassAmount.text = "" + GameManager.Instance.gainedValues.biomass;
        if(rationsAmount != null) rationsAmount.text = "" + GameManager.Instance.gainedValues.rations;
        if(soilAmount != null) soilAmount.text = "" + GameManager.Instance.gainedValues.soil;
//        if(textAir != null) textAir.text = "" + GameManager.Instance.gainedValues.air;
    }
}
