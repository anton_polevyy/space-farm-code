﻿/*
 *    !!!!!! READ ME !!!!
 *
 *     !!!! Important:
 *     Note that the "Sort Order" number of this Canvas
 *     should be higher than of every other Canvases.
 *     Otherwise player may not be able to press the "DestroySelf" button
 *     as it can get underneath another Panel.
 *
 * 
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PopUpMessageUI : MonoBehaviour
{
    
    public TextMeshProUGUI headerField;
    public TextMeshProUGUI titleField;
    public TextMeshProUGUI textField;
    [Header("Destroy Current PopUp Message")]
    public Button btnDestroySelf;
    
    
    // Start is called before the first frame update
    void Start()
    {
        btnDestroySelf.onClick.AddListener(delegate { Destroy(gameObject); });
    }

    // Update is called once per frame
    void Update()
    {
//        Canvas[] = FindObjectOfType(Canvas);
    }

    public void SetHeader(string txt)
    {
        headerField.SetText(txt);
    }
    
    public void SetTitle(string txt)
    {
        titleField.SetText(txt);
    }
    
    public void SetMessage(string txt)
    {
        textField.SetText(txt);
    }
}
