﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GuiManager : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, 
	IBeginDragHandler, IDragHandler, IEndDragHandler
{

	[Header("Object to move around (obj with camera)")] 
	public GameObject player;

	
	public float maxY = 120f;
	public float minY = 10f;
	public float rotationSpeed = 10.0f;
	public float upDownRange = 30.0f;
	public float maxAngleVert = 70f;
	public float minAngleVert = 0f;
	
	[Header("Double Tap option")]
	public float clickdelay = 0.5f;
	private float clicked = 0;
	private float clickTime = 0;
	


	[Header("to highlight selected object")] 
	public Color colorSelected = Color.green;
	public CameraOutlineRenderer.SortingType depthType = CameraOutlineRenderer.SortingType.Overlay;
	
	[Header("GUI Element with Item Menu")]
	public GameObject itemBuildingUI;
	public GameObject itemDroidUI;
	public GameObject itemLabUI;
	public GameObject researchLabCanvas;
	
	[Header("to target Drone to the Point on Ground")]
	public string layerGround = "Ground";
	public LayerMask layersOfGround;
	public string layerOfObject = "Resource";
	public LayerMask layersOfTargetObjects;
	
	[Header("to show Messages")]
	public GameObject messagePanel;
	public float messageShowForSeconds = 2;
	
	private Camera camera;
	private CameraOutlineRenderer outlineHighlighter;
	private GameObject currentGameObject;
	private bool isLookingDroneTarget;
	
	private int layerGroundNumber;
	private int layerMask;
	private Vector3 worldPoint;
	private int layerObjectNumber;
	
	
	
	private float yaw = 0.0f;	// rotation Y (horizontal)
	private float pitch = 0.0f;	// rotation X (vertical)
	private float pitchZero = 0.0f;	// center for the vertical angle (varies from minAngleVert to maxAngleVert)  
	
	private Vector3 pointerReference;
	private Vector3 pointerOffset;
	private Vector3 rotation;

	private Vector2 pointerPrePosition;
	
	
	
	
	
	
	void Start () {
		// get camera
		camera = Camera.main;
		outlineHighlighter = camera.GetComponent<CameraOutlineRenderer>();
		
		// get the number of layer named "Ground" to use for raycast filtering
		layerGroundNumber = LayerMask.NameToLayer(layerGround);
		// Bit shift the index of the layer "Ground" to get a bit mask
		layerMask = 1 << layerGroundNumber;

		layerObjectNumber = LayerMask.NameToLayer(layerOfObject);
		
		// just a precaution
		if (maxY - minY == 0) maxY += 1;
		pitch = player.transform.position.y * (maxAngleVert - minAngleVert) / (maxY - minY);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	
	
	// -----------------------------------------------------------
	// -------------------- POINTER functions
	// -----------------------------------------------------------
	

	public void OnBeginDrag(PointerEventData pointer)
	{
//		Debug.Log("OnBeginDrag()");

		pointerPrePosition = pointer.position;
//		Debug.Log("pointerPrePosition = " + pointer.position);
	}

	public void OnDrag(PointerEventData pointer)
	{
//		Debug.Log("OnDrag()");
//
//		Rotate(pointer);
////		UpdateRotation();

	}

	public void OnEndDrag(PointerEventData pointer)
	{
//		Debug.Log("OnEndDrag()");
	}

	
	public void OnPointerDown(PointerEventData pointer)
	{
		HideItemUIs();
		
		
//		/* ---------- Default option for double tap/click ----------- */
//		//Grab the number of consecutive clicks and assign it to an integer variable.
//		int i = pointer.clickCount;
//		//Display the click count.
//		Debug.Log("pointer clicked " + i + " times");
		
		
//		/* ---------- Manual option for double tap/double click ----------- */
//		/* ---------- where you can specify the delay time ----------- */
//		clicked++;
//		if (clicked == 1)
//		{
//			Debug.Log("1st click");
//			clickTime = Time.time;
//		}
// 
//		if (clicked > 1 && Time.time - clickTime < clickdelay)
//		{
//			clicked = 0;
//			clickTime = 0;
//			Debug.Log("Double CLick: ");
//			ShowItemUI();
// 
//		}
//		else if (clicked > 2 || Time.time - clickTime > 1) clicked = 0;
		
		

		


		/* ---------- Logic ----------- */
		
		// Clear the highlight outline on every tap
		if (currentGameObject != null) outlineHighlighter.ClearOutlineData();
		
		// if not drone looking target yet do next 
		if (!isLookingDroneTarget)
		{
			currentGameObject = GetObjectOnPointer(pointer);
			Debug.Log("currentGameObject is " + currentGameObject);
			
			InventoryItem itemScript = currentGameObject.GetComponent<InventoryItem>();
			if (itemScript != null)
			{
				Debug.Log("hoghlightimh Inventory Item");
				HighlightObject(currentGameObject);
			}

			ItemDroid droidScript = currentGameObject.GetComponent<ItemDroid>();
			if (droidScript != null)
			{
				Debug.Log("GUI: click on droid");
				isLookingDroneTarget = true;
			}

		}
		// if drone is looking target, then next click assigns target
		else
		{
			Debug.Log("is Looking Drone Target");
			isLookingDroneTarget = false;
			
			// for Droid: Change State
			ItemDroid itemDroidScript = currentGameObject.GetComponent<ItemDroid>();
			GameObject targetObject = GetObjectOnPointer(pointer);
			
			if (targetObject != null && (layersOfGround & (1<<targetObject.layer)) == 0)
			{
				Debug.Log("targetObject " + targetObject + ", targetObject.layer = " + targetObject.layer);
				itemDroidScript.GoToObject(targetObject);
			}
			else
			{
				worldPoint = GetPointerToGroundPosition(pointer.position);
			
				// check if proper target point was found or not
				if (worldPoint.y == -1000.0f)
				{
					Debug.Log("pointer clicked not on the layerGround. Drone can't go there :(");
//					isLookingDroneTarget = false;
					return;
				}
				else
				{
					itemDroidScript.GoToPosition(worldPoint);
				}
			}
			
			currentGameObject = null;
		}
		
		
		/* ----------------------------------- Right Mouse Button ----------------------- */

		if (pointer.button == PointerEventData.InputButton.Right) {
//			Debug.Log ("Right Mouse Button Clicked ");
			ShowItemUI();
		}

	}
	
	
	public void OnPointerClick(PointerEventData pointer)
	{

	}
	
	public void OnPointerUp(PointerEventData pointer)
	{
		
	}



	
	
	// -----------------------------------------------------------
	// -------------------- ROTATION of drone when touching base station
	// -----------------------------------------------------------
	void UpdateRotation()
	{
		float h = player.transform.position.y;
		pitchZero = h * (maxAngleVert - minAngleVert) / (maxY - minY);
		
		pitch -= Input.GetAxis("Mouse Y") * rotationSpeed;
		yaw += Input.GetAxis("Mouse X") * rotationSpeed;
		
		// restrict vertical rotation angle
//		pitch = Mathf.Clamp(pitch, pitchZero - upDownRange, pitchZero + upDownRange);
		
		//Update camera rotation
		player.transform.localRotation = Quaternion.Euler(pitch, yaw, 0);
	}
	
	private void Rotate(PointerEventData pointer)
	{
		Vector2 posVector = pointer.position - pointerPrePosition;
		pointerOffset = new Vector3(posVector.x, posVector.y, 0.0f);
		pointerPrePosition = pointer.position;
	}
	
	
	
	
	
	

	public void OnDestroyButton()
	{
		bool isInInventoryManager = false;
		
		foreach (GameObject obj in InventoryManager.Instance.items)
		{
			if (obj == currentGameObject)
			{
//				Debug.Log("found maching current in Inventory List");
				isInInventoryManager = true;
				InventoryManager.Instance.items.Remove(obj);
				Destroy(currentGameObject);
				HideItemUIs();
				return;
			}
		}

		if (!isInInventoryManager)
		{
			HideItemUIs();
			Destroy(currentGameObject);
		}
	}

	public void Recycle()
	{
		InventoryItem itemScript = currentGameObject.GetComponent<InventoryItem>();
		
		GameManager.Instance.gainedValues += itemScript.returnOnRecycle;
		OnDestroyButton();
	}


	/*
	 * --------------- Drone UI --------------------------
	 */
	
	public void OnDroneTargetButton()
	{
		isLookingDroneTarget = true;
		HideItemUIs();
	}


	
	
	/*
	 * --------------- Lab UI --------------------------
	 */
	
	private IEnumerator ShowMessagePanelForSeconds()
	{
		messagePanel.SetActive(true);
		yield return new WaitForSeconds(messageShowForSeconds);
		messagePanel.SetActive(false);
	}

	public void OnLabProcessIceButton()
	{
		ItemLab itemScript = (ItemLab) currentGameObject.GetComponent<ItemLab>();
		if (itemScript == null)
		{
			HideItemUIs();
			return;
		}
		
		int processCost = itemScript.iceAmount;
		Debug.Log("to process Ice you need amount of " + processCost);
		if (processCost <= GameManager.Instance.gainedValues.ice)
		{
			GameManager.Instance.gainedValues.ice -= processCost;
			itemScript.ProcessIce();
		}
		else
		{
			Debug.Log("not enough resources for this item");
			
			if (messagePanel != null) StartCoroutine(ShowMessagePanelForSeconds());
		}
		
		HideItemUIs();
	}
	
	public void OnLabProcessRocksButton()
	{
		ItemLab itemScript = (ItemLab) currentGameObject.GetComponent<ItemLab>();
		if (itemScript == null)
		{
			HideItemUIs();
			return;
		}
		
		int processCost = itemScript.rocksAmount;
		Debug.Log("to process Ice you need amount of " + processCost);
		if (processCost <= GameManager.Instance.gainedValues.rocks)
		{
			GameManager.Instance.gainedValues.rocks -= processCost;
			itemScript.ProcessRocks();
		}
		else
		{
			Debug.Log("not enough resources for this item");
			
			if (messagePanel != null) StartCoroutine(ShowMessagePanelForSeconds());
		}
		
		HideItemUIs();
	}
	
	public void OnLabProcessMineralsButton()
	{
		ItemLab itemScript = (ItemLab) currentGameObject.GetComponent<ItemLab>();
		if (itemScript == null)
		{
			HideItemUIs();
			return;
		}
		
		int processCost = itemScript.mineralsAmount;
		Debug.Log("to process Ice you need amount of " + processCost);
		if (processCost <= GameManager.Instance.gainedValues.minerals)
		{
			GameManager.Instance.gainedValues.minerals -= processCost;
			itemScript.ProcessMinerals();
		}
		else
		{
			Debug.Log("not enough resources for this item");
			
			if (messagePanel != null) StartCoroutine(ShowMessagePanelForSeconds());
		}
		
		HideItemUIs();
	}
	

	public void OnOpenLabPanelBtn()
	{
		GameManager.Instance.researchLab = currentGameObject;
		
//		ItemLabAnaliticsUI script = (ItemLabAnaliticsUI)researchLabCanvas.GetComponent(typeof(ItemLabAnaliticsUI));
//		if (script != null)
//		{
//			Debug.Log("passing currentLab to script");
//			script.SetCurrentLab(currentGameObject);			
//		}
//		else
//		{
//			Debug.Log("Error: could not find script to pass ItemLab there");
//		}
	}


	/*
	 * --------------- All Item UIs --------------------------
	 */
	
	// add here all UIs that attached to GameObjects in the World
	private void HideItemUIs()
	{
		itemBuildingUI.SetActive(false);
		itemLabUI.SetActive(false);
		itemDroidUI.SetActive(false);
	}


	private GameObject GetObjectOnPointer(PointerEventData pointer)
	{
		GameObject obj = null;
		
		Vector3 pointerPos = new Vector3(pointer.position.x, pointer.position.y, 0);
		
		Vector3 hitPoint = Vector3.zero;
		RaycastHit hit = new RaycastHit();
		Ray ray = camera.ScreenPointToRay(pointerPos);

		if (Physics.Raycast (ray, out hit, Mathf.Infinity))
		{
			obj = hit.transform.gameObject;
		}

		return obj;
	}
	
	
	public bool IsPointerOnUIObject(PointerEventData pointer, GameObject uiObject)
	{
		GameObject obj = null;
		
		Vector3 pointerPos = new Vector3(pointer.position.x, pointer.position.y, 0);
		
		GraphicRaycaster graphicRaycaster;
        
		//Fetch the Raycaster from the GameObject (the Canvas)
		graphicRaycaster = GetComponent<GraphicRaycaster>();
		Debug.Log("graphicRaycaster " + graphicRaycaster);
        
		//Create a list of Raycast Results
		List<RaycastResult> results = new List<RaycastResult>();
        
		//Raycast using the Graphics Raycaster and mouse click position
		graphicRaycaster.Raycast(pointer, results);

		//For every result returned, output the name of the GameObject on the Canvas hit by the Ray
		foreach (RaycastResult result in results)
		{
			Debug.Log("Hit " + result.gameObject.name);
			if (result.gameObject == uiObject)
			{
				Debug.Log("Found matching UI " + result.gameObject.name);
				return true;
			}
		}
        
		return false;
	}

//	public bool IsPointerOnItemObject()
//	{
//		GameObject obj = null;
//		
//		PointerEventData pointer = new PointerEventData(EventSystem.current);
//		
//		Vector3 pointerPos = new Vector3(pointer.position.x, pointer.position.y, 0);
//		
//		GraphicRaycaster graphicRaycaster;
//        
//		//Fetch the Raycaster from the GameObject (the Canvas)
//		graphicRaycaster = GetComponent<GraphicRaycaster>();
//		Debug.Log("graphicRaycaster " + graphicRaycaster);
//        
//		//Create a list of Raycast Results
//		List<RaycastResult> results = new List<RaycastResult>();
//        
//		//Raycast using the Graphics Raycaster and mouse click position
//		graphicRaycaster.Raycast(pointer, results);
//
//		//For every result returned, output the name of the GameObject on the Canvas hit by the Ray
//		foreach (RaycastResult result in results)
//		{
//			Debug.Log("Hit " + result.gameObject.name);
//			InventoryItem itemScript = result.gameObject.GetComponent<InventoryItem>();
//			if (itemScript != null)
//			{
//				Debug.Log("Pointing to Inventory Item object " + result.gameObject.name);
//				return true;
//			}
//		}
//        
//		return false;
//	}

	
	/*
	 * return point under the pointer (mouse/ finger) that hits the layerGround
	 */
	private Vector3 GetPointerToGroundPosition(Vector2 pointerPos)
	{
//		Vector3 hitPoint = Vector3.zero;
		Vector3 hitPoint = new Vector3(0.0f, -1000.0f, 0.0f); // height -1000 as a triger that position wasn't find
		RaycastHit hit = new RaycastHit();
		Ray ray = camera.ScreenPointToRay(new Vector3(pointerPos.x, pointerPos.y, 0.0f));

		if (Physics.Raycast (ray, out hit, Mathf.Infinity, layerMask)) {
			hitPoint = hit.point;
		}

		return hitPoint;
	}


	public void ShowItemUI()
	{
		InventoryItem itemScript = (InventoryItem) currentGameObject.GetComponent<InventoryItem>();
		if (itemScript != null)
		{
			if (itemScript.GetType() == typeof(ItemBuilding))
			{
				Debug.Log("it's a building");
				itemBuildingUI.transform.position = itemScript.GetUIPosition();
				itemBuildingUI.SetActive(true);
			}
			else if (itemScript.GetType() == typeof(ItemLab))
			{
				Debug.Log("it's a lab");
				itemLabUI.transform.position = itemScript.GetUIPosition();
				itemLabUI.SetActive(true);
			}
			else if (itemScript.GetType() == typeof(ItemDroid))
			{
				Debug.Log("it's a droid");
				itemDroidUI.transform.position = itemScript.GetUIPosition();
				itemDroidUI.SetActive(true);
			}
		}
	}
	
	
	/*
	 * --------------- Build Droid --------------------------
	 */
	public void OnBuildDroidBtn()
	{
		// check if there are enough of resources
		
		
		// create droid in front of the camera
	}


	
	/*
	 * --------------- Outline Highlighter --------------------------
	 */

	private void HighlightObject(GameObject obj)
	{
		List<Renderer> renderers = new List<Renderer>();
		foreach (Renderer renderer in obj.GetComponentsInChildren<Renderer>())
			renderers.Add(renderer);

		outlineHighlighter.AddRenderers(renderers, colorSelected);
	}


}
