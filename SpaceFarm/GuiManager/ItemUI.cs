﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemUI : MonoBehaviour
{

//	public GameObject itemUI;
	public GameObject attachUIToObject;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
//	void Update ()
//	{
//		if (itemUI.activeSelf) UpdateUIPosition();
//		
//	}

	public Vector3 GetUIPosition()
	{
		if (attachUIToObject != null)
			return Camera.main.WorldToScreenPoint(attachUIToObject.transform.position);
		else return Camera.main.WorldToScreenPoint(gameObject.transform.position);
	}

//	private void UpdateUIPosition()
//	{
//		itemUI.transform.position = Camera.main.WorldToScreenPoint(attachUIToObject.transform.position);
//	}
	

}
