﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEditor;


[System.Serializable]
public struct MissionButton
{
    public Button button;
    public Image indicator;
}

public class MissionUI : MonoBehaviour
{
    public Color incompleteMissionColor;
    public Color completeMissionColor;
    public Color currentMissionColor;
    [Header("Show Pop Up Message On Mission Done")]
    public GameObject popUpMessagePrefab;
//    public GameObject popUpMessagePanel;
//    public TextMeshProUGUI popUpHeaderField;
//    public TextMeshProUGUI popUpTitleField;
//    public TextMeshProUGUI popUpTextField;
    
    [Header("Mission Log")]
    public TextMeshProUGUI missionName;
    public Image indicatorMissionName;
    public TextMeshProUGUI missionTitle;
    public TextMeshProUGUI missionDescription;
    public Image indicatorMissionDescription;
    public TextMeshProUGUI missionRequirements;
    public Image indicatorMissionRequirements;
    [Header("Mission Buttons")] 
    public List<MissionButton> missionButtons;
   
    
    [HideInInspector]
    public List<bool> missionWasDone;    // keeps record of the missions that have been done at least once


    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < MissionManager.Instance.missionIsDone.Count; i++)
            missionWasDone.Add(false);

        for (int i = 0; i < missionButtons.Count; i++)
            missionButtons[i].indicator.color = incompleteMissionColor;
        
        for (int i = 0; i < missionButtons.Count; i++)
        {
            int closureIndex = i ; // Prevents the closure problem
            missionButtons[closureIndex].button.onClick.AddListener(delegate
            {
                ShowMissionDescription(MissionManager.Instance.missions[closureIndex]);
                ShowMissionIndicator(closureIndex);
            });
        }

    }

    // Update is called once per frame
    void Update()
    {
        int i = GetFirstlyCompletedMission();
        if (i != -1)
        {
//            missionWasDone[i] = true;
//            missionButtons[i].indicator.color = completeMissionColor;
            ShowPopUpMessage(MissionManager.Instance.missions[i]);
        }

        UpdateMissionIndicators();
    }
    
    private void ShowMissionDescription(Mission mission)
    {
        missionName.SetText(mission.name);
        missionTitle.SetText(mission.title);
        missionDescription.SetText(mission.description);
        missionRequirements.SetText(mission.requirements);
    }

    private void ShowMissionIndicator(int index)
    {        
        indicatorMissionName.color = missionButtons[index].indicator.color;
        indicatorMissionDescription.color = missionButtons[index].indicator.color;
        indicatorMissionRequirements.color = missionButtons[index].indicator.color;
    }
    
    private int GetFirstlyCompletedMission()
    {
//        int i = 0;
//        while (i < missionWasDone.Count)
//        {
//            if (missionWasDone[i] != MissionManager.Instance.missionIsDone[i])
//            {
////                isMissionsDone[i] = MissionManager.Instance.missionIsDone[i];
//                return i;
//            }
//            i++;
//        }

        for (int i = 0; i < missionWasDone.Count; i++)
        {
            if (!missionWasDone[i] && missionWasDone[i] != MissionManager.Instance.missionIsDone[i])
            {
                missionWasDone[i] = true;
                return i;
            }
        }
        return -1;
    }

    private void ShowPopUpMessage(Mission mission)
    {
//        popUpHeaderField.SetText(mission.name);
//        popUpTitleField.SetText(mission.title);
//        popUpTextField.SetText(mission.endText);
//        popUpMessagePanel.SetActive(true);

        GameObject popUpMessage = Instantiate(popUpMessagePrefab);
        PopUpMessageUI popUpScript = popUpMessage.GetComponent<PopUpMessageUI>();
        popUpScript.SetHeader(mission.name);
        popUpScript.SetTitle(mission.title);
        popUpScript.SetMessage(mission.endText);
    }

    private void UpdateMissionIndicators()
    {
        for (int i = 0; i < missionButtons.Count; i++)
        {
            missionButtons[i].indicator.color = (MissionManager.Instance.missionIsDone[i])
                ? completeMissionColor
                : incompleteMissionColor;
        }
    }

}
