﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

using UnityEngine;

public interface IResourceGenerator
{
   GameObject GetProductionPad();
   Vector3 GetResourcePosition();
}
