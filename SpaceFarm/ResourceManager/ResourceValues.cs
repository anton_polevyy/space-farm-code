﻿/*
 *	READ ME
 *
 * current structure has Comparison Operators:
 * ==	- all parameters are equal
 * !=	- at leat one parameter is not equal
 * >	- all parameters of left variable are more then parameters of right variable
 * <	- all parameters of left variable are less then parameters of right variable
 * >=	- all parameters of left variable are more or equal then parameters of right variable
 * <=	- all parameters of left variable are less or equal then parameters of right variable
 * 
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ResourceValues {
	
	[Header("Raw Resources")] 
	public int ice;
	public int rocks;
	public int minerals;

    [Header( "Simple Refined Resources")]
    public int water;
    public int nitrogen;
    public int oxygen;
    public int carbon;
    public int metal;

    [Header( "Complex Refined Resources")]
    public int biomass;
    public int rations;
    public int soil;
    public int air;
	public int heat;



	public ResourceValues(int i = 0, int rk = 0, int mn = 0, int w = 0, int n = 0, int o = 0, int c = 0, int m = 0, 
		int b = 0, int r = 0, int s = 0, int a = 0, int h = 0)
	{
		
		ice = i;
		rocks = rk;
		minerals = mn;
		
		water = w;
		nitrogen = n;
		oxygen = o;
		carbon = c;
		metal = m;
	
		biomass = b;
		rations = r;
		soil = s;
		air = a;
		heat = h;
	}
	
	
	/*
	 * ----------------	Comparison Operators -------------------------------
	 */
	
	public static bool operator ==(ResourceValues v1, ResourceValues v2)
	{
		return (v1.water == v2.water) && (v1.nitrogen == v2.nitrogen) 
		                              && (v1.oxygen == v2.oxygen) 
		                              && (v1.carbon == v2.carbon)
		                              && (v1.metal == v2.metal)
		                              && (v1.biomass == v2.biomass) 
		                              && (v1.rations == v2.rations) 
		                              && (v1.soil == v2.soil) 
		                              && (v1.air == v2.air)
		                              && (v1.ice == v2.ice)
		                              && (v1.rocks == v2.rocks)
		                              && (v1.minerals == v2.minerals)
		                              && (v1.heat == v2.heat);
	}
	
	public static bool operator !=(ResourceValues v1, ResourceValues v2)
	{
		return (v1.water != v2.water) || (v1.nitrogen != v2.nitrogen) 
		                              || (v1.oxygen != v2.oxygen) 
		                              || (v1.carbon != v2.carbon)
		                              || (v1.metal != v2.metal)
		                              || (v1.biomass != v2.biomass) 
		                              || (v1.rations != v2.rations) 
		                              || (v1.soil != v2.soil) 
		                              || (v1.air != v2.air)
		                              || (v1.ice != v2.ice)
		                              || (v1.rocks != v2.rocks)
		                              || (v1.minerals != v2.minerals)
		                              || (v1.heat != v2.heat);
	}
	
    public static bool operator >(ResourceValues v1, ResourceValues v2)
    {	
	    return (v1.water > v2.water) && (v1.nitrogen > v2.nitrogen) 
	                                 && (v1.oxygen > v2.oxygen) 
	                                 && (v1.carbon > v2.carbon)
	                                 && (v1.metal > v2.metal)
	                                 && (v1.biomass > v2.biomass) 
	                                 && (v1.rations > v2.rations) 
	                                 && (v1.soil > v2.soil) 
	                                 && (v1.air > v2.air)
	                                 && (v1.ice > v2.ice)
	                                 && (v1.rocks > v2.rocks)
	                                 && (v1.minerals > v2.minerals)
	                                 && (v1.heat > v2.heat);
    }
	
    public static bool operator <(ResourceValues v1, ResourceValues v2)
    {
	    return (v1.water < v2.water) && (v1.nitrogen < v2.nitrogen) 
	                                 && (v1.oxygen < v2.oxygen) 
	                                 && (v1.carbon < v2.carbon)
	                                 && (v1.metal < v2.metal)
	                                 && (v1.biomass < v2.biomass) 
	                                 && (v1.rations < v2.rations) 
	                                 && (v1.soil < v2.soil) 
	                                 && (v1.air < v2.air)
	                                 && (v1.ice < v2.ice)
	                                 && (v1.rocks < v2.rocks)
	                                 && (v1.minerals < v2.minerals)
	                                 && (v1.heat < v2.heat);
    }
	
	public static bool operator >=(ResourceValues v1, ResourceValues v2)
	{
		return (v1.water >= v2.water) && (v1.nitrogen >= v2.nitrogen) 
		                              && (v1.oxygen >= v2.oxygen) 
		                              && (v1.carbon >= v2.carbon)
		                              && (v1.metal >= v2.metal)
		                              && (v1.biomass >= v2.biomass) 
		                              && (v1.rations >= v2.rations) 
		                              && (v1.soil >= v2.soil) 
		                              && (v1.air >= v2.air)
		                              && (v1.ice >= v2.ice)
		                              && (v1.rocks >= v2.rocks)
		                              && (v1.minerals >= v2.minerals)
		                              && (v1.heat >= v2.heat);
		
	}
	
	public static bool operator <=(ResourceValues v1, ResourceValues v2)
	{
		return (v1.water <= v2.water) && (v1.nitrogen <= v2.nitrogen) 
		                              && (v1.oxygen <= v2.oxygen) 
		                              && (v1.carbon <= v2.carbon)
		                              && (v1.metal <= v2.metal)
		                              && (v1.biomass <= v2.biomass) 
		                              && (v1.rations <= v2.rations) 
		                              && (v1.soil <= v2.soil) 
		                              && (v1.air <= v2.air)
		                              && (v1.ice <= v2.ice)
		                              && (v1.rocks <= v2.rocks)
		                              && (v1.minerals <= v2.minerals)
		                              && (v1.heat <= v2.heat);
	}
	
	/*
	 * ----------------	Arithmetic Operators -------------------------------
	 */
	
	public static ResourceValues operator +(ResourceValues v1, ResourceValues v2)
	{
		ResourceValues sum = new ResourceValues();

		sum.water = v1.water + v2.water;
		sum.nitrogen = v1.nitrogen + v2.nitrogen;
		sum.oxygen = v1.oxygen + v2.oxygen;
		sum.carbon = v1.carbon + v2.carbon;
		sum.metal = v1.metal + v2.metal;
		sum.biomass = v1.biomass + v2.biomass;
		sum.rations = v1.rations + v2.rations;
		sum.soil = v1.soil + v2.soil;
		sum.air = v1.air + v2.air;
		sum.ice = v1.ice + v2.ice;
		sum.rocks = v1.rocks + v2.rocks;
		sum.minerals = v1.minerals + v2.minerals;
		sum.heat = v1.heat + v2.heat;
		
		return sum;
	}
	
	public static ResourceValues operator -(ResourceValues v1, ResourceValues v2)
	{
		ResourceValues sum = new ResourceValues();

		sum.water = v1.water - v2.water;
		sum.nitrogen = v1.nitrogen - v2.nitrogen;
		sum.oxygen = v1.oxygen - v2.oxygen;
		sum.carbon = v1.carbon - v2.carbon;
		sum.metal = v1.metal - v2.metal;
		sum.biomass = v1.biomass - v2.biomass;
		sum.rations = v1.rations - v2.rations;
		sum.soil = v1.soil - v2.soil;
		sum.air = v1.air - v2.air;
		sum.ice = v1.ice - v2.ice;
		sum.rocks = v1.rocks - v2.rocks;
		sum.minerals = v1.minerals - v2.minerals;
		sum.heat = v1.heat - v2.heat;
		
		return sum;
	}
	
	/*
	 * ----------------	Other Functions -------------------------------
	 */

	public bool hasNegative()
	{
		return (water < 0) || (nitrogen < 0) 
		                   || (oxygen < 0) 
		                   || (carbon < 0)
		                   || (metal < 0)
		                   || (biomass < 0) 
		                   || (rations < 0) 
		                   || (soil < 0) 
		                   || (air < 0)
						   || (ice < 0) 
						   || (rocks < 0) 
						   || (minerals < 0)
		                   || (heat < 0);
	}
}
