﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
//using UnityEngine.UI; 

//[System.Serializable]
//public struct PadProductionTime
//{
//	public GameObject pad;
//	public GameObject resourcePrefab;
//	public float seconds;
//}

[System.Serializable]
public class ItemLab : InventoryItem, IResourceGenerator {

//	public string name;

	/*
	 Extraction of Elements from Raw Resources:
		ice 		-> Water, Oxygen (, Nitrogen)
		rocks 		-> Carbon, Nitrogen
		minerals	-> Carbon, Metal
	*/
//	public List<PadProductionTime> iceProcessPads;
//	public List<PadProductionTime> rocksProcessPads;
//	public List<PadProductionTime> mineralsProcessPads;

	[Header("Ice -> Water & Oxygen")] 
	public int iceAmount = 10;
//	public Slider waterOxygenSlider;
	public PadProductionTime waterProduction;
	public PadProductionTime oxygenProduction;
	
	[Header("Rocks -> Carbon & Nitrogen")] 
	public int rocksAmount = 10;
//	public Slider carbonNitrogenSlider;
	public PadProductionTime carbonProduction;
	public PadProductionTime nitrogenProduction;
	
	[Header("Minerals -> Carbon & Metals")] 
	public int mineralsAmount = 10;
//	public Slider carbonMetalSlider;
	public PadProductionTime carbonProduction2;
	public PadProductionTime metalsProduction;
	
	
	private List<PadProductionTime> productionsList = new List<PadProductionTime>();

	
	public float waterToOxygenRatio { set; get; }
	public float oxygenToWaterRatio { set; get; }
	public float carbonToNitrogenRatio { set; get; }
	public float nitrogenToCarbonRatio { set; get; }
	public float carbonToMetalsRatio { set; get; }
	public float metalsToCarbonRatio { set; get; }

	// Use this for initialization
	void Start ()
	{

		waterToOxygenRatio = 0.5f;
		oxygenToWaterRatio = 0.5f;
		carbonToNitrogenRatio = 0.5f;
		nitrogenToCarbonRatio = 0.5f;
		carbonToMetalsRatio = 0.5f;
		metalsToCarbonRatio = 0.5f;
		
		if (waterProduction.pad != null) productionsList.Add(waterProduction);
		if (oxygenProduction.pad != null) productionsList.Add(oxygenProduction);
		if (carbonProduction.pad != null) productionsList.Add(carbonProduction);
		if (nitrogenProduction.pad != null) productionsList.Add(nitrogenProduction);
		if (carbonProduction2.pad != null) productionsList.Add(carbonProduction2);
		if (metalsProduction.pad != null) productionsList.Add(metalsProduction);
		
	}
	
	// Update is called once per frame
	void Update ()
	{
//		Debug.Log("water-oxygen slider value: " + waterOxygenSlider.value);
	}

	
	
	
	
	IEnumerator Produce(PadProductionTime prod) 
	{
		yield return new WaitForSeconds(prod.seconds);
		if (prod.resourcePrefab != null)
		{
			Vector3 position = prod.pad.transform.position;
			Instantiate(prod.resourcePrefab, position, Quaternion.identity);
			Debug.Log("Produced resource " + prod.resourcePrefab);
		}
	}


	public void ProcessIce()
	{
//		foreach (PadProductionTime prod in iceProcessPads)
//		{
//			StartCoroutine(Produce(prod));
//		}
		
		
//		int waterFromIceAmount = (int) (waterToOxygenRatio * iceAmount);
//		int oxygenFromIceAmount = (int) (oxygenToWaterRatio * iceAmount);
		
		int waterFromIceAmount = (int) (Mathf.Round(waterToOxygenRatio * 10f) / 10f * iceAmount);
		int oxygenFromIceAmount = (int) (Mathf.Round(oxygenToWaterRatio * 10f) / 10f * iceAmount);
		
		
		Debug.Log("Processing Ice: water " + waterFromIceAmount + ", oxygen " + oxygenFromIceAmount);
		
		for (int i = 0; i < waterFromIceAmount; i++) StartCoroutine(Produce(waterProduction));
		for (int i = 0; i < oxygenFromIceAmount; i++) StartCoroutine(Produce(oxygenProduction));
	}
	
	public void ProcessRocks()
	{
		int carbonFromRocksAmount = (int) (Mathf.Round(carbonToNitrogenRatio * 10f) / 10f * rocksAmount);
		int nitrogenFromRocksAmount = (int) (Mathf.Round(nitrogenToCarbonRatio * 10f) / 10f * rocksAmount);
		
		Debug.Log("Processing Rocks: carbon " + carbonFromRocksAmount + ", nitrogen " + nitrogenFromRocksAmount);

		for (int i = 0; i < carbonFromRocksAmount; i++) StartCoroutine(Produce(carbonProduction));
		for (int i = 0; i < nitrogenFromRocksAmount; i++) StartCoroutine(Produce(nitrogenProduction));
	}
	
	public void ProcessMinerals()
	{
		int carbonFromMineralsAmount = (int) (Mathf.Round(carbonToMetalsRatio * 10f) / 10f * mineralsAmount);
		int metalsFromMineralsAmount = (int) (Mathf.Round(metalsToCarbonRatio * 10f) / 10f * mineralsAmount);
		
		Debug.Log("Processing Minerals: carbon " + carbonFromMineralsAmount + ", metals " + metalsFromMineralsAmount);

		for (int i = 0; i < carbonFromMineralsAmount; i++) StartCoroutine(Produce(carbonProduction2));
		for (int i = 0; i < metalsFromMineralsAmount; i++) StartCoroutine(Produce(metalsProduction));
	}


	/*
	 * ------------- Functions For Resource Production Pads -----------------------------
	 */
	
	
	public GameObject GetProductionPad()
	{
		// return pad, which is  first to produce a resource cube
		GameObject productionPad = null;
		productionPad = GetPadWithMostResources();
		return productionPad;
	}

	private GameObject GetPadWithMostResources()
	{
		GameObject richPad = null;
		int maxNumber = 0;

		foreach (PadProductionTime production in productionsList)
		{
			int numberOfCollisions = CountCollisionsOfPad(production.pad);
//			Debug.Log("pad " + production.pad + ", collisions number " + numberOfCollisions);
			if (maxNumber <= numberOfCollisions)
			{
				richPad = production.pad;
				maxNumber = numberOfCollisions;
			}
		}

		return richPad;
	}

	public Vector3 GetResourcePosition()
	{
		return GetProductionPad().transform.position;
	}


	private int CountCollisionsOfPad(GameObject pad)
	{
		Collider collider = pad.GetComponent<Collider>();
		Vector3 extents = collider.bounds.extents;
		
		RaycastHit hit = new RaycastHit();
		
		//Use the OverlapBox to detect if there are any other colliders within this box area.
		Collider[] hitColliders = Physics.OverlapBox(pad.transform.position, extents, pad.transform.rotation);

		return hitColliders.Length;
	}

}
