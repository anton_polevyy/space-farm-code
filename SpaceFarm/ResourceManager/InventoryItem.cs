﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

using System;
using System.Reflection;


/*
 * Inventory Value on building has negative value
 * and on resource cube it has positive value
 */



public class InventoryItem : MonoBehaviour
{
	public string name;
	
	public ResourceValues resourceCost;
	public ResourceValues returnOnRecycle;
	
	public GameObject attachUIToObject;
	
	
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public bool isAvailable()
	{
//		if (resourceCost.hasNegative()) 
		return (resourceCost <= GameManager.Instance.gainedValues);
	}
	
	public Vector3 GetUIPosition()
	{
		if (attachUIToObject != null)
			return Camera.main.WorldToScreenPoint(attachUIToObject.transform.position);
		else return Camera.main.WorldToScreenPoint(gameObject.transform.position);
	}

//	public void Recycle()
//	{
//		GameManager.Instance.gainedValues += returnOnRecycle;
//		Destroy(gameObject);
//	}

}
