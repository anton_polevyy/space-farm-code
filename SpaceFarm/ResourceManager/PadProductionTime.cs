﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PadProductionTime
{
    public GameObject pad;
    public GameObject resourcePrefab;
    public float seconds;
}
