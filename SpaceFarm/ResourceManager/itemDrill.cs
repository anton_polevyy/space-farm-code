﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemDrill : InventoryItem
{
	[Header("If attached to Ice")] 
	public string iceTag = "Ice";
	public PadProductionTime iceProcessPad;
	
	[Header("If attached to Rocks")] 
	public string rocksTag = "Rocks";
	public PadProductionTime rocksProcessPad;
	
	[Header("If attached to Minerals")] 
	public string mineralsTag = "Minerals";
	public PadProductionTime mineralsProcessPad;

	private PadProductionTime processPad;

	// Use this for initialization
	void Start ()
	{
		// assign processPad based on the tag of colliding object
		string tag = GetColliderTag();
//		Debug.Log("drill's collider tag is " + tag);

		if (tag == iceTag) processPad = iceProcessPad;
		if (tag == rocksTag) processPad = rocksProcessPad;
		if (tag == mineralsTag) processPad = mineralsProcessPad;
		
		InvokeRepeating("ProduceResources", processPad.seconds, processPad.seconds);
	}
	
	// Update is called once per frame
	void Update ()
	{
	}

	public Vector3 GetResourcePosition()
	{
		return processPad.pad.transform.position;
	}

	public GameObject GetProductionPad()
	{
		return processPad.pad;
	}

	private void ProduceResources()
	{
//		produced += production;
//		Debug.Log("produced.carbon = " + produced.carbon);

		if (processPad.pad == null || processPad.resourcePrefab == null) return;
		Vector3 position = GetResourcePosition();
		Instantiate(processPad.resourcePrefab, position, Quaternion.identity);
	}

	private string GetColliderTag()
	{
		string tag = "";
		
		Collider collider = gameObject.GetComponent<Collider>();
		Vector3 extents = collider.bounds.extents;
		RaycastHit hit = new RaycastHit();
		
		//Use the OverlapBox to detect if there are any other colliders within this box area.
		Collider[] hitColliders = Physics.OverlapBox(gameObject.transform.position, extents, gameObject.transform.rotation);

		foreach (Collider col in hitColliders)
		{
			
//			Debug.Log("drill's colliding with " + col.tag);
			if (col.tag == iceTag)
			{
				return iceTag;
			}
			
			if (col.tag == rocksTag)
			{
				return rocksTag;
			}
			
			if (col.tag == mineralsTag)
			{
				return mineralsTag;
			}
		}

		return tag;
	}
}
