﻿/*
 * Standart OnCollisionEnter(...) can miss a collision if a small object
 * gets inside of current object too quick
 * 
 * Therefore, it's better to use Physics.OverlapBox(...)
 * as it scans if any object is inside the current object 
 * 
 */



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBaseStation : MonoBehaviour {

	public LayerMask destroyObjectOfLayer;

	private BoxCollider[] boxColliders;
	
	
	void Awake () {
		boxColliders = GetComponents<BoxCollider>();
	}
	
	// Update is called once per frame
	void Update () {
		for(int i = 0; i < boxColliders.Length; i++)
		{
			DeleteDetectedObjects(boxColliders[i]);
		}
	}

	private void DeleteDetectedObjects(BoxCollider boxCollider)
	{
		Vector3 dimensions = new Vector3(boxCollider.bounds.extents.x * 2, boxCollider.bounds.extents.y * 2, boxCollider.bounds.extents.z * 2);
        
		//Use the OverlapBox to detect if there are any other colliders within this box area.
		Collider[] hitColliders = Physics.OverlapBox(boxCollider.bounds.center, dimensions, Quaternion.identity, destroyObjectOfLayer);

		for (int i = 0; i < hitColliders.Length; i++)
		{			
			ResourceCube script = (ResourceCube) hitColliders[i].gameObject.GetComponent<ResourceCube>();
			if (script != null)
			{
				Debug.Log("Base Station: got the resource cube");
				GameManager.Instance.gainedValues += script.resourceValues;
				Destroy(hitColliders[i].gameObject);
			}
		}
	}
}
