﻿/*
 *	
 * 
 * 
 */




using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Panda;


public class ItemDroid : InventoryItem
{
    [Header("Droid Settings")] 
    public float droidSpeed = 7;
    public float rotateToTargetSpeed = 3f;
    public float angleApproximation = 10.0f;
    [Tooltip("consider the position has been reached at distance")]
    public float distanceApproximation = 0.3f;
    public float detectAtDistance = 10;
    [Tooltip("Distance to stop in front of the destination object")]
    public float stopAtDistance = 3f;
    public float castRadius = 1;

    [Header("Carry Resource Setting")] 
    public GameObject baseStation;
    public LayerMask layerToCarry;
    public float holdDistance = 4f;
    
    [Header("Gizmos Settings")]
    public Color idleColor = Color.yellow;
    public Color foundTargetColor = Color.red;
    
    
    [HideInInspector] public NavMeshAgent navMeshAgent;
    [HideInInspector] public Vector3 destinationPosition;
    [HideInInspector] public GameObject targetObject;
    [HideInInspector] public GameObject carryObject;
    
    //Booleans
    [HideInInspector] private bool canSeeTargetObject;    // used only to DrawGizmos
    [HideInInspector] private bool isTouchingTargetObject;
    

    
    #region tasks

    [Task] private bool isFacingObject;
    // Status Flags
    [Task] bool taskIdle;
    [Task] private bool taskGoToPosition;
    [Task] private bool taskGoToObject;
    [Task] private bool taskCarryResourceToBase;

    /*
     * --------------------- Go To Position -----------------------
     */

    [Task]
    void SetDestinationPosition()
    {
        navMeshAgent.destination = destinationPosition;
        Task.current.Succeed();
    }

    [Task]
    void MoveToPosition()
    {
        navMeshAgent.Resume();
        Task.current.Succeed();
    }
    
    [Task]
    bool HasComeToPosition()
    {
        return (navMeshAgent.remainingDistance < distanceApproximation && !navMeshAgent.pathPending);
    }

    [Task]
    void StopMoving()
    {
        navMeshAgent.Stop();
        Task.current.Succeed();
        Debug.Log("StopMovingToPosition");
    }

    [Task]
    void FinishTaskGoToPosition()
    {
        taskGoToPosition = false;
        Debug.Log("FinishTaskGoToPosition()");
    }

    
    /*
     * --------------------- Go To Object -----------------------
     */
    
    [Task]
    public void UpdateDestinationWhenSeeObject()
    {
        // raycast in front to detect object before collision
        RaycastHit hit = new RaycastHit();
        if (Physics.SphereCast(transform.position, castRadius, transform.TransformDirection(Vector3.forward), out hit,
                detectAtDistance)
            && hit.transform.gameObject == targetObject)
        {
            canSeeTargetObject = true;
            destinationPosition = Vector3.MoveTowards(hit.point, transform.position, stopAtDistance);
//            navMeshAgent.destination = destinationPosition;
            Debug.Log("Detected Object via Raycasting: my pos = " + transform.position 
                       + ", object's wall position = " + hit.point 
                       + ", new destination = " + destinationPosition);
            Task.current.Succeed();
        }else 
            canSeeTargetObject = false;
    }

    [Task]
    public void UpdateDestinationWhenCollidedWithObject()
    {
        if (isTouchingTargetObject)
        {
            isTouchingTargetObject = false;
            destinationPosition = Vector3.MoveTowards(transform.position, targetObject.transform.position, -stopAtDistance);
//            navMeshAgent.destination = destinationPosition;
            Debug.Log("Detected Object via Collision: my pos = collision pos = " + transform.position 
                      + ", target pos = " + targetObject.transform.position 
                      + ", new destination = " + destinationPosition);
            Task.current.Succeed();
        }
    }
    
    
    [Task]
    void StopMovingToObject()
    {
        Task.current.Succeed();
        Debug.Log("StopMovingToObject()");
    }

    [Task]
    void FinishTaskGoToObject()
    {
        taskGoToObject = false;
        targetObject = null;
        isTouchingTargetObject = false;
        Debug.Log("FinishTaskGoToObject()");
    }


    /*
     * --------------------- Rotate To Object -----------------------
     */

    [Task]
    public bool IsFacingObject()
    {
        Vector3 direction = targetObject.transform.position - transform.position;
        // it's important to set y = 0 for both vectors to level them horizontally
        float angle = Vector3.Angle(new Vector3(direction.x, 0, direction.z), new Vector3(transform.forward.x, 0, transform.forward.z));
        
        if (angle > angleApproximation)
        {
//            Debug.Log("Droid: facing the object NOT..." + "angle = " + angle);
            return false;
        }
        else
        {
//            Debug.Log("Droid: facing the object YES..." + "angle = " + angle);
            return true;
        }
    }

    [Task]
    public void RotateToObject()
    {
        // perform gradual rotation
        Vector3 direction = (targetObject.transform.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotateToTargetSpeed);
//        Debug.Log("RotateToObject(): transform.rotation = " + transform.rotation);
    }

    [Task]
    void StopRotatingToObject()
    {
        Task.current.Succeed();
        Debug.Log("StopRotatingToObject()");
    }
    
    
    /*
     * --------------------- Carry Resource Object -----------------------
     */

    [Task]
    void SetCarryObject()
    {
        carryObject = targetObject;
        Task.current.Succeed();
    }

    [Task]
    void SetDestinationToBase()
    {
        targetObject = baseStation;
        destinationPosition = targetObject.transform.position;
        Task.current.Succeed();
    }

    [Task]
    bool IsCarrying()
    {
//        Debug.Log("carryObject = " + carryObject);
        return carryObject != null;
    }

    [Task]
    void CarryResource()
    {
        carryObject.transform.position = gameObject.transform.position + gameObject.transform.forward * holdDistance;
//        Task.current.Succeed();
    }

    [Task]
    void FinishCarryResource()
    {
        Debug.Log("FinishCarryResource()");
        Task.current.Succeed();
    }

    [Task]
    void FinishTaskCarryResourceToBase()
    {
        Debug.Log("FinishTaskCarryResourceToBase()");
        taskCarryResourceToBase = false;
        Task.current.Succeed();
    }


    #endregion
    
    

    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();

        navMeshAgent.speed = droidSpeed;

        taskIdle = true;
        taskGoToPosition = false;
    }
    
    void Update()
    {
        UpdateStatuses();
//        BeIdleIfNoTasks();
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = idleColor;
        
        if (canSeeTargetObject)
        {
            Gizmos.color = foundTargetColor;
        }
        
        Gizmos.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * detectAtDistance);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject == targetObject)
        {
            isTouchingTargetObject = true;
            Debug.Log("isTouchingTargetObject = " + isTouchingTargetObject);
        }
    }

    public void GoToPosition(Vector3 position)
    {
        ResetTasks();
        ResetVariables();
        taskGoToPosition = true;
        
        destinationPosition = position;        
        Debug.Log("GoToPosition(): destinationPosition = " + destinationPosition);
    }


    public void GoToObject(GameObject obj)
    {
        ResetTasks();
        ResetVariables();

        if ((layerToCarry & (1<<obj.layer)) != 0)
        {
            GetResourceAndCarryToBase(obj);
        }
        else
        {
            taskGoToObject = true;
        
            targetObject = obj;
            destinationPosition = targetObject.transform.position;
            Debug.Log("GoToObject(): taskGoToObject = " + taskGoToObject);
        }
    }

    private void GetResourceAndCarryToBase(GameObject obj)
    {
        ResetTasks();
        ResetVariables();
        taskCarryResourceToBase = true;

        targetObject = obj;
        destinationPosition = targetObject.transform.position;
        Debug.Log("GetResourceAndCarryToBase(): taskCarryResourceToBase = " + taskCarryResourceToBase);
    }


    private void UpdateStatuses()
    {
        if (!taskGoToPosition && !taskGoToObject)
        {
            taskIdle = true;
        }
        else taskIdle = false;
    }

    private void BeIdleIfNoTasks()
    {
        if (!taskGoToPosition && !taskGoToObject)
        {
            taskIdle = true;
        }
        else taskIdle = false;
    }

    private void ResetTasks()
    {
        taskIdle = false;
        taskGoToPosition = false;
        taskGoToPosition = false;
        taskCarryResourceToBase = false;
    }

    private void ResetVariables()
    {
        targetObject = null;
        carryObject = null;
        isTouchingTargetObject = false;

    }

}
