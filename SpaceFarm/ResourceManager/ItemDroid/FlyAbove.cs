﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyAbove : MonoBehaviour
{

	public string layerGround = "Ground";

//	public float thrust = 1.0f;
//	public float upForce = 3.0f;
	public float uplift = 2.0f;
//	public Rigidbody rb;

	private int layerGroundNumber;
	private int layerMask;
	private Vector3 groundPoint;
	
	// Use this for initialization
	void Start () {
		// get the number of layer named "Ground" to use for raycast filtering
		layerGroundNumber = LayerMask.NameToLayer(layerGround);
		// Bit shift the index of the layer "Ground" to get a bit mask
		layerMask = 1 << layerGroundNumber;
		
//		rb = GetComponent<Rigidbody>();
	}
	
	
	void Update ()
	{

		groundPoint = GetGroundPoint();
//		Vector3 offset = groundPoint - transform.position;
//		Debug.Log("offset " + offset);
//		float dist = transform.position.y - groundPoint.y;
//		float fracDist = dist / uplift;
		
		transform.position = groundPoint + new Vector3(0.0f, uplift, 0.0f);
	}
	
//	void FixedUpdate(){
//		float yVel = rigid.velocity.y + Physics.gravity.y;
// 
//		//Howering
//		rigid.AddForce(0, -yVel, 0, ForceMode.Acceleration);
// 
//		//Altitude
//		rigid.AddForce(0, Input.GetAxis("Vertical") * 5, 0);
//	}
	
	
	// Get the point on the ground straight underneath object
	Vector3 GetGroundPoint()
	{
		RaycastHit hit = new RaycastHit();
		Vector3 hitPoint = new Vector3(transform.position.x, 0.0f, transform.position.y);
		
		if (Physics.Raycast (transform.position, -Vector3.up, out hit, Mathf.Infinity, layerMask)) {
//			float distance = hit.distance;
			hitPoint = hit.point;
		} else if (Physics.Raycast (transform.position, Vector3.up, out hit, Mathf.Infinity, layerMask)) {
			hitPoint = hit.point;
		}
		
		return hitPoint;
	}
}
