﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBuilding : InventoryItem, IResourceGenerator
{
//	public string name;
//	public ResourceValues production;
	public float productionForSeconds = 10;
	public GameObject productionPrefab;
	public GameObject padToProduce;

	private ResourceValues produced;

	// Use this for initialization
	void Start ()
	{
		InvokeRepeating("ProduceResources", productionForSeconds, productionForSeconds);
	}
	
	// Update is called once per frame
	void Update ()
	{
	}

	public Vector3 GetResourcePosition()
	{
		return padToProduce.transform.position;
	}

	public GameObject GetProductionPad()
	{
		return padToProduce;
	}

	private void ProduceResources()
	{
//		produced += production;
//		Debug.Log("produced.carbon = " + produced.carbon);

		if (productionPrefab == null) return;
		Vector3 position = GetResourcePosition();
		Instantiate(productionPrefab, position, Quaternion.identity);
	}
}
