﻿/*
 *	CONTROLS:
 * 
 *	W, A, S, D - horizontal movements
 *	Mouse Scroll - Height ajastment
 * 	Right Mouse hold and drag - turn left-right and look up-down
 *
 *  Addition: Physics.Raycast(...) doesn't allow to go lower than the terrain
 * 			and when hit the ground rigidbody.useGravity activates to prevent you from going through hills
 * 
 */





using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementControl : MonoBehaviour {

	public float moveSpeed = 10f;
	public float maxY = 120f;
	public float minY = 10f;
	public float scrollSpeed = 100f;

	public float upDownRange = 30.0f;
	public float yRotationSpeed = 100f;
	public float xRotationSpeed = 10f;
	public float maxAngleVert = 70f;
	public float minAngleVert = 0f;

	public string layerGround = "Ground";
	public float stayHigherThanTerrain = 1.0f;
    
	
	private Vector2 screenCenter = new Vector2(0.0f, 0.0f);
	private float yaw = 0.0f;	// rotation Y (horizontal)
	private float pitch = 0.0f;	// rotation X (vertical)
	private float pitchZero = 0.0f;	// center for the vertical angle (varies from minAngleVert to maxAngleVert)   
	
	private Rigidbody rigidbody;
	private int groundLayer;		// number of layer named "Ground"
	private Vector3 groundPoint = Vector3.zero;	// point on the ground underneath you
	
	
	void Start () {
		
		// just a precaution
		if (maxY - minY == 0) maxY += 1;
		pitch = transform.position.y * (maxAngleVert - minAngleVert) / (maxY - minY);
		
		// get the center of the screen
		screenCenter.x = Screen.width / 2;
		screenCenter.y = Screen.height / 2;
		
		// rigid body is used to walk on hills in terrain
		rigidbody = GetComponent<Rigidbody>();
//		if (!rigidbody) Debug.Log("No rigitbody");
		
		// get the number of layer named "Ground" to use for raycast filtering
		groundLayer = LayerMask.NameToLayer(layerGround);
	}
	
	void Update () {
		
		UpdateRotation();
		UpdatePosition();

		groundPoint = GetGroundPoint();
		StayOnGroud(); // prevents from falling through the terrain
	}

	
	// -----------------------------------------------------------
	// -------------------- ROTATION 
	// -----------------------------------------------------------
	void UpdateRotation()
	{
		float h = transform.position.y;
		pitchZero = h * (maxAngleVert - minAngleVert) / (maxY - minY);
		
		if (Input.GetMouseButton(1) || Input.GetMouseButton(2))
		{
			pitch -= Input.GetAxis("Mouse Y") * xRotationSpeed;
			yaw += Input.GetAxis("Mouse X") * xRotationSpeed;
			
//			// another way of horizontal rotation
//			if (Input.mousePosition.x - screenCenter.x > 0)
//			{
//				yaw += yRotationSpeed * Time.deltaTime;
//			}
//			else
//			{
//				yaw -= yRotationSpeed * Time.deltaTime;
//			}	
		}
		
		// restrict vertical rotation angle
		pitch = Mathf.Clamp(pitch, pitchZero - upDownRange, pitchZero + upDownRange);
		
		//Update camera rotation
		transform.localRotation = Quaternion.Euler(pitch, yaw, 0);
	}

	// -----------------------------------------------------------
	// -------------------- POSITION 
	// -----------------------------------------------------------
	void UpdatePosition()
	{
		// Get current position
		Vector3 pos = transform.position;
		float posY = pos.y;
		
		// Moving
		if ( Input.GetKey(KeyCode.W) )
		{
			pos += transform.forward * Time.deltaTime * moveSpeed;
		}
		if ( Input.GetKey(KeyCode.A) )
		{
			pos -= transform.right * moveSpeed * Time.deltaTime;
		}
		if ( Input.GetKey(KeyCode.S) )
		{
			pos -= transform.forward * moveSpeed * Time.deltaTime;
		}
		if ( Input.GetKey(KeyCode.D) )
		{
			pos += transform.right * moveSpeed * Time.deltaTime;
		}

		//Zooming (changing height)
		float scroll = Input.GetAxis("Mouse ScrollWheel");
		posY -= scroll * scrollSpeed * Time.deltaTime;

		//Clamp height
//		posY = Mathf.Clamp(posY, minY, maxY);
		posY = (posY > maxY) ? maxY : posY;
		
		//Update object position
		transform.position = new Vector3(pos.x, posY, pos.z);
	}
	
	
	
	
	
	
	// -----------------------------------------------------------
	// -------------------- COLLISION
	// -----------------------------------------------------------
	void OnCollisionEnter(Collision collision)
	{
		rigidbody.useGravity = true;
	}

	private void OnCollisionExit(Collision other)
	{
		rigidbody.useGravity = false;
	}


	// Get the point on the ground straight underneath you
	Vector3 GetGroundPoint()
	{
		RaycastHit hit = new RaycastHit();
		Vector3 hitPoint = new Vector3(transform.position.x, 0.0f, transform.position.y);
		// Bit shift the index of the layer "Ground" to get a bit mask
		int layerMask = 1 << groundLayer;
		
		if (Physics.Raycast (transform.position, -Vector3.up, out hit, Mathf.Infinity, layerMask)) {
			float distance = hit.distance;
			hitPoint = hit.point;
		} else if (Physics.Raycast (transform.position, Vector3.up, out hit, Mathf.Infinity, layerMask)) {
			hitPoint = hit.point;
		}
		
		return hitPoint;
	}

	// make sure that the current height is no lower than the terrain
	void StayOnGroud()
	{
		Vector3 pos = transform.position;
		if (pos.y < groundPoint.y + stayHigherThanTerrain)
		{
			pos.y = groundPoint.y + stayHigherThanTerrain;
//			float d = (groundPoint.y + stayHigherThanTerrain) - pos.y;
//			pos.y += d * 10 * Time.deltaTime;
		}
		transform.position = pos;
	}

}
